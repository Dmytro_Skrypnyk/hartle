require "rails_helper"

RSpec.describe "routes for Static Page", :type => :routing do
  it "routes /static page to the home controller" do
    expect(get("/")).
      to route_to("static_pages#home")
  end

  it "help /static page to the help controller" do
    expect(get("/help")).
      to route_to("static_pages#help")
  end

  it "about /static page to the about controller" do
    expect(get("/about")).
      to route_to("static_pages#about")
  end

  it "contact /static page to the contact controller" do
    expect(get("/contact")).
      to route_to("static_pages#contact")
  end  

end