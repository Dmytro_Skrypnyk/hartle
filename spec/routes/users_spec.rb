require "rails_helper"

RSpec.describe "routes for Users", :type => :routing do
  it "singup / page to the singup controller" do
    expect(get("/signup")).
      to route_to("users#new")
  end

  it "new / user page to the new controller" do
    expect(get("/login")).
      to route_to("sessions#new")
  end

  it "create / page to the create controller" do
    expect(get("/login")).
      to route_to("sessions#new")
  end

  it "destroy / user page to the destroy controller" do
    expect(delete("/logout")).
      to route_to("sessions#destroy")
  end
  
end
