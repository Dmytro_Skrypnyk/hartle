require "rails_helper"

RSpec.describe UsersController, :type => :controller do
  before { @user = User.create(name: "Dmytro",
                            email: "Skrypnyk_d@ukr.net",
                            password: "123456") }
  describe 'POST #create' do
    context 'when user was valid' do
      before do
        @before_count = User.count
        @before_user = User.last
        post :create, params: {
          user: {
            name:  "Test",            
            email: "test@test.com",            
            password: '111111'            
          }
        }
      end
      it 'creates new user' do
        expect(User.count).to_not be eq(@before_count)
      end
      it 'redirects to user\'s page' do
        expect(response).to redirect_to(User.last)
      end
      it 'not to redirect to new' do
        user = User.last
        user.name = "SomeName"
        expect(user.save).to redirect_to(User.last)
      end
      it 'it shoul redirect to new' do
        user = User.last
        user.name = nil       
        expect(user.save).to redirect_to(User.last)
      end
    end
  end 
  describe 'POST #edit' do
    context 'when user was change' do
      before do
        @before_edit = User.first 
      end
      it 'change user' do
        User.first.save
        expect(User.first).to_not be eq(@before_edit)
      end
    end
  end
end


