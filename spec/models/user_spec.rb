require "rails_helper"

RSpec.describe "Test for Model User", :type => :model do
  
  describe 'Validation of Users' do
    before do
      @test_user = User.create(name: "Dmytro",
                           email: "Skrypnyk_d@ukr.net",
                           password: "123456")      
        @before_count = User.count

        @user = User.new(name: "Dmytro",
                           email: "Skrypnyk_dm@ukr.net",
                           password: "123456")
    end
    context 'when user is valid' do      
      it "it should be balid if all values is valid" do
        @user.save
        expect(User.count).not_to be eq(@before_count)        
      end
      it "it should have valid name" do
        @user.name = "example_name"
        expect(@user).to be_valid
      end
      it "it should have presence name" do
        @user.name = "     "
        expect(@user).not_to be_valid
      end
      it "shoud have not to long name (les 50)" do
        @user.name = "a" * 51
        expect(@user).not_to be_valid
      end
      it "can have name (equel 50)" do
        @user.name = "a" * 50
        expect(@user).to be_valid
      end
      it "it should have valid email" do
        @user.email = "example!email.com"
        expect(@user).not_to be_valid        
      end
      it "it should have valid email" do
        @user.email = "example@email.com"
        expect(@user).to be_valid        
      end
      it "should have uniq email" do
        @user.email = "Skrypnyk_d@ukr.net"
        expect(@user).not_to be_valid
      end
      it "should have not to long email" do
        @user.email = "a"*255 + "@gmail.com"
        expect(@user).not_to be_valid
      end
      it "should have long email (less or equal 255)" do
        @user.email = "a"*245 + "@gmail.com"
        expect(@user).to be_valid
      end
      it "shoud have not to short password" do
        @user.password = "a" * 5
        expect(@user).not_to be_valid
      end
      it "can have long password (equel 50)" do
        @user.password = "a" * 50
        expect(@user).to be_valid
      end
    end    
  end
end
